

//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/customactionfilter/ActionFilterHelper.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #2 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __ActionFilterHelper_H__
#define __ActionFilterHelper_H__

// General Includes:
#include "K2TypeTraits.h"
#include "K2Vector.h"
#include "K2Vector.tpp"
#include "OMTypes.h"
#include "PMString.h"

// Interface Includes:
#include "IActionComponent.h"

// default values, or Don't Care values
/** Default value for ClassIDs */
const ClassID kClassIDDONTCARE = kInvalidClass;
/** Default value for ActionIDs */
const ActionID kActionIDDONTCARE = kInvalidActionID;
/** Default value for PMStrings */
const PMString kActionStringDONTCARE = "";
/** Default value for bool16 */
const bool16 kUserEditableDONTCARE = -1; 
/** Default value for int16 */
const int16 kActionTypeDONTCARE = -1; 
/** Default value for unit32 */
const uint32 kEnablingTypeDONTCARE = 0xFFFFFFFF;



/** A helper class where you can specify how to filter an action. 
 *  
 * 	Using this class:
 * 
 * 	<B>UseCase</B>: Specifying what actions to filter<BR>
 * 
 *  1. Create a new instance of this class.<BR>
 * 	2. Call necessary Set*() methods to specify what you want to filter.<BR>
 * 	3. If you have multiple actions to filter, repeat steps (1) and (2) and store 
 * 		them all into a K2Vector.<BR>
 *  <BR>
 * 	Sample:
<PRE>K2Vector&lt;ActionFilterHelper&gt; actionFilters;

// filter helper for specific actionID, actionName, and actionComponent
ActionFilterHelper filter1;
// filter the ActionID: the last kTrue param indicates this is a filter "key", 
// which is what the IsMatch() will key off of. Keep ActionID the same.
filter1.SetFilterActionID(kTrue, kFooOldActionID, kFooOldActionID, kTrue);
// filter the ActionName
filter1.SetFilterActionName(kTrue, PMString("FooOldActionName"), PMString("FooNewActionName")); 
// filter the component class: we can now handle this ActionID in the kFooNewActionComponentBoss!
filter1.SetFilterComponentClass(kTrue, kFooOldActionComponentBoss, kFooNewActionComponentBoss);
// add to the vector
actionFilters.push_back(filter1);

// another filter helper...
ActionFilterHelper filter2;
...
// add to the vector
actionFilters.push_back(filter2);
</PRE>
 * 
 * 	<B>UseCase</B>: Filtering actions inside an ActionFilter implementation<BR>
 * 
 * 	1. Get an instance of this class.<BR>
 * 	2. Call IsMatch() to see if the IActionFilter::FilterAction() inputs match the criterion.<BR>
 *     - If yes, then call DoFilter(). <BR>
 * 	   - If you have multiple actions to filter, repeat steps (1)-(3) from a K2Vector. <BR>
 *  <BR>
 * 	Sample:
<PRE>K2Vector&lt;ActionFilterHelper&gt;::iterator iter = actionFilterList.begin();
while (iter != actionFilterList.end())
{
	if (iter->IsMatch(...)) // only one should match
	{
		iter->DoFilter(...);
		break;
	}
	iter++;
}
</PRE>
 *  
 * @ingroup customactionfilter
 * @author Ken Sadahiro
*/
class ActionFilterHelper
{
public:
	/** Constructor. Initializes member variables.
	 */
	ActionFilterHelper(void)
	{
		this->fFilterKey = kKeyDONTCARE;
		this->fOrigActionComponent = nil;
		this->SetFilterComponentClass(kFalse);
		this->SetFilterActionID(kFalse);
		this->SetFilterActionName(kFalse);
		this->SetFilterActionArea(kFalse);
		this->SetFilterActionType(kFalse);
		this->SetFilterEnablingType(kFalse);
		this->SetFilterUserEditable(kFalse);
	}

	/** Copy constructor.
	 * 	@param helper (in) Reference to another ActionFilterHelper object.
	*/
	ActionFilterHelper(const ActionFilterHelper& helper)
	{
		this->fFilterKey = helper.fFilterKey;
		this->fOrigActionComponent = helper.fOrigActionComponent;
		this->SetFilterComponentClass(helper.fFilterEnabled_ComponentClass, 
									  helper.fOrigComponentClass, 
									  helper.fNewComponentClass, 
									  (fFilterKey == helper.kKeyComponentClass) ? kTrue:kFalse);
		this->SetFilterActionID(helper.fFilterEnabled_ActionID, 
								helper.fOrigActionID, 
								helper.fNewActionID, 
								(fFilterKey == helper.kKeyActionID) ? kTrue:kFalse);
		this->SetFilterActionName(helper.fFilterEnabled_ActionName, 
								  helper.fOrigActionName, 
								  helper.fNewActionName, 
								  (fFilterKey == helper.kKeyActionName) ? kTrue:kFalse);
		this->SetFilterActionArea(helper.fFilterEnabled_ActionArea, 
								  helper.fOrigActionArea, 
								  helper.fNewActionArea, 
								  (fFilterKey == helper.kKeyActionArea) ? kTrue:kFalse);
		this->SetFilterActionType(helper.fFilterEnabled_ActionType, 
								  helper.fOrigActionType, 
								  helper.fNewActionType, 
								  (fFilterKey == helper.kKeyActionType) ? kTrue:kFalse);
		this->SetFilterEnablingType(helper.fFilterEnabled_EnablingType, 
									helper.fOrigEnablingType, 
									helper.fNewEnablingType, 
									(fFilterKey == helper.kKeyEnablingType) ? kTrue:kFalse);
		this->SetFilterUserEditable(helper.fFilterEnabled_UserEditable, 
									helper.fOrigUserEditable, 
									helper.fNewUserEditable, 
									(fFilterKey == helper.kKeyUserEditable) ? kTrue:kFalse);
	}

	/** Destructor.
	 */
	~ActionFilterHelper(void)
	{
		// release the IActionComponent
		if (fOrigActionComponent != nil)
		{
			fOrigActionComponent->Release();
			fOrigActionComponent = nil;
		}
	}

	/** Enumeration that specifies on which field to key the filter.
	 * 	Each value corresponds to a parameter in the 
	 * 	IActionFilter::FilterAction() method.
	*/
	typedef enum
	{
		kKeyDONTCARE = 0,
		kKeyComponentClass,
		kKeyActionID,
		kKeyActionName, 
		kKeyActionArea, 
		kKeyActionType, 
		kKeyEnablingType, 
		kKeyUserEditable
	}Key;

	// Set methods - use these to specify what you want to filter.

	/** Sets the filter settings for the ComponentClass.
	 * 	@param enableFilter (in) Set to kTrue to enable this filter (which would store these values)
	 * 	@param origComponentClass (in) Specify the original component class to look for
	 * 	@param newComponentClass (in) Specify the new component class to filter to.
	 * 	@param isKey (in) Set to kTrue if this is what you want to key off of in the 
	 * 		IActionFilter::FilterAction() method. Only one value can be a key.
	 */
	void SetFilterComponentClass(const bool16 enableFilter, 
								 const ClassID& origComponentClass = kClassIDDONTCARE, 
								 const ClassID& newComponentClass = kClassIDDONTCARE, 
								 const bool16 isKey = kFalse);

	/** Sets the filter settings for ActionID.
	 * 	@param enableFilter (in) Set to kTrue to enable this filter (which would store these values)
	 * 	@param origActionID (in) Specify the original ActionID to look for
	 * 	@param newActionID (in) Specify the new ActionID to filter to.
	 * 	@param isKey (in) Set to kTrue if this is what you want to key off of in the 
	 * 		IActionFilter::FilterAction() method. Only one value can be a key.
	 */
	void SetFilterActionID(const bool16 enableFilter, 
						   const ActionID& origActionID = kActionIDDONTCARE, 
						   const ActionID& newActionID = kActionIDDONTCARE, 
						   const bool16 isKey = kFalse);

	/** Sets the filter settings for ActionName.
	 * 	@param enableFilter (in) Set to kTrue to enable this filter (which would store these values)
	 * 	@param origActionName (in) Specify the original ActionName to look for
	 * 	@param newActionName (in) Specify the new ActionName to filter to.
	 * 	@param isKey (in) Set to kTrue if this is what you want to key off of in the 
	 * 		IActionFilter::FilterAction() method. Only one value can be a key.
	 */
	void SetFilterActionName(const bool16 enableFilter, 
							 const PMString& origActionName = kActionStringDONTCARE, 
							 const PMString& newActionName = kActionStringDONTCARE, 
							 const bool16 isKey = kFalse);

	/** Sets the filter settings for ActionArea.
	 * 	@param enableFilter (in) Set to kTrue to enable this filter (which would store these values)
	 * 	@param origActionArea (in) Specify the original ActionArea to look for
	 * 	@param newActionArea (in) Specify the new ActionArea to filter to.
	 * 	@param isKey (in) Set to kTrue if this is what you want to key off of in the 
	 * 		IActionFilter::FilterAction() method. Only one value can be a key.
	 */
	void SetFilterActionArea(const bool16 enableFilter, 
							 const PMString& origActionArea = kActionStringDONTCARE, 
							 const PMString& newActionArea = kActionStringDONTCARE, 
							 const bool16 isKey = kFalse);

	/** Sets the filter settings for ActionType.
	 * 	@param enableFilter (in) Set to kTrue to enable this filter (which would store these values)
	 * 	@param origActionType (in) Specify the original ActionType to look for
	 * 	@param newActionType (in) Specify the new ActionType to filter to.
	 * 	@param isKey (in) Set to kTrue if this is what you want to key off of in the 
	 * 		IActionFilter::FilterAction() method. Only one value can be a key.
	 */
	void SetFilterActionType(const bool16 enableFilter, 
							 const int16 origActionType = kActionTypeDONTCARE, 
							 const int16 newActionType = kActionTypeDONTCARE, 
							 const bool16 isKey = kFalse);

	/** Sets the filter settings for EnablingType.
	 * 	@param enableFilter (in) Set to kTrue to enable this filter (which would store these values)
	 * 	@param origEnablingType (in) Specify the original EnablingType to look for
	 * 	@param newEnablingType (in) Specify the new EnablingType to filter to.
	 * 	@param isKey (in) Set to kTrue if this is what you want to key off of in the 
	 * 		IActionFilter::FilterAction() method. Only one value can be a key.
	 */
	void SetFilterEnablingType(const bool16 enableFilter, 
							   const uint32 origEnablingType = kEnablingTypeDONTCARE, 
							   const uint32 newEnablingType = kEnablingTypeDONTCARE, 
							   const bool16 isKey = kFalse);

	/** Sets the filter settings for UserEditable.
	 * 	@param enableFilter (in) Set to kTrue to enable this filter (which would store these values)
	 * 	@param origUserEditable (in) Specify the original UserEditable to look for
	 * 	@param newUserEditable (in) Specify the new UserEditable to filter to.
	 * 	@param isKey (in) Set to kTrue if this is what you want to key off of in the 
	 * 		IActionFilter::FilterAction() method. Only one value can be a key.
	 */
	void SetFilterUserEditable(const bool16 enableFilter, 
							   const bool16 origUserEditable = kUserEditableDONTCARE, 
							   const bool16 newUserEditable = kUserEditableDONTCARE, 
							   const bool16 isKey = kFalse);

	/** Based on the filter settings, it performs the filtering.
	 * 	@see IActionFilter::FilterAction()
	 */
	void DoFilter(ClassID* componentClass, 
				  ActionID* actionID, 
				  PMString* actionName, 
				  PMString* actionArea, 
				  int16* actionType, 
				  uint32* enablingType, 
				  bool16* userEditable);


	// Get/Query methods

	/** Indicates if the given parameters are a match for the filter specification
	 * 	set by the Set methods. 
	 * 	@return Returns kTrue if the filter key matches the corresponding passed-in 
	 * 		parameter. Returns kFalse otherwise.  
	 * 		For instance, if the key is set to kKeyActionID, and you are want to
	 * 		filter kCloseActionID to kMyCloseActionID, and if the actionID parameter
	 * 		passed into this function is kCloseActionID, that is considered a match,
	 * 		regardless of what the other parameters are.
	 * 	@see IActionFilter::FilterAction()
	 */
	bool16 IsMatch(const ClassID& componentClass, 
				   const ActionID& actionID, 
				   const PMString& actionName, 
				   const PMString& actionArea,
				   const int16 actionType, 
				   const uint32 enablingType, 
				   const bool16 userEditable);

	/** Queries the original action component for the given action ID.
	 * 	@return Original IActionComponent.  
	 * 		If fOrigComponentClass is DONTCARE, this returns nil. 
	 */
	IActionComponent* QueryOrigActionComponent(void);

	/** Gets the filter key. */
	inline Key GetFilterKey(void) 
	{
		return this->fFilterKey;
	}

	/** Returns kTrue if this helper filters the component class.
	 */
	inline bool16 IsFilterComponentClassEnabled(void)
	{
		return this->fFilterEnabled_ComponentClass;
	}

	/** Returns kTrue if this helper filters the action ID.
	 */
	inline bool16 IsFilterActionIDEnabled(void)
	{
		return this->fFilterEnabled_ActionID;
	}

	/** Returns kTrue if this helper filters the action name.
	*/
	inline bool16 IsFilterActionNameEnabled(void)
	{
		return this->fFilterEnabled_ActionName;
	}

	/** Returns kTrue if this helper filters the action area.
	*/
	inline bool16 IsFilterActionAreaEnabled(void)
	{
		return this->fFilterEnabled_ActionArea;
	}

	/** Returns kTrue if this helper filters the action type.
	*/
	inline bool16 IsFilterActionTypeEnabled(void)
	{
		return this->fFilterEnabled_ActionType;
	}

	/** Returns kTrue if this helper filters the enabling type.
	*/
	inline bool16 IsFilterEnablingTypeEnabled(void)
	{
		return this->fFilterEnabled_EnablingType;
	}

	/** Returns kTrue if this helper filters the user editable option.
	*/
	inline bool16 IsFilterUserEditableEnabled(void)
	{
		return this->fFilterEnabled_UserEditable;
	}

	/** Gets the component class (original and new)
	 */
	void GetFilterComponentClass(ClassID& origComponentClass, ClassID& newComponentClass)
	{
		origComponentClass = fOrigComponentClass;
		newComponentClass = fNewComponentClass;
	}

	/** Gets the action ID (original and new)
	 */
	void GetFilterActionID(ActionID& origActionID, ActionID& newActionID)
	{
		origActionID = fOrigActionID;
		newActionID = fNewActionID;
	}

	/** Gets the action name (original and new)
	*/
	void GetFilterActionName(PMString& origActionName, PMString& newActionName)
	{
		origActionName = fOrigActionName;
		newActionName = fNewActionName;
	}

	/** Gets the action area (original and new)
	*/
	void GetFilterActionArea(PMString& origActionArea, PMString& newActionArea)
	{
		origActionArea = fOrigActionArea;
		newActionArea = fNewActionArea;
	}

	/** Gets the action type (original and new)
	*/
	void GetFilterActionType(int16& origActionType, int16& newActionType)
	{
		origActionType = fOrigActionType;
		newActionType = fNewActionType;
	}

	/** Gets the enabling type (original and new)
	*/
	void GetFilterEnablingType(uint32& origEnablingType, uint32& newEnablingType)
	{
		origEnablingType = fOrigEnablingType;
		newEnablingType = fNewEnablingType;
	}

	/** Gets the user editable option (original and new)
	*/
	void GetFilterUserEditable(bool16& origUserEditable, bool16& newUserEditable)
	{
		origUserEditable = fOrigUserEditable;
		newUserEditable = fNewUserEditable;
	}

private:

	Key fFilterKey;

	IActionComponent* fOrigActionComponent;

	bool16 fFilterEnabled_ComponentClass;
	ClassID fOrigComponentClass, fNewComponentClass;

	bool16 fFilterEnabled_ActionID;
	ActionID fOrigActionID, fNewActionID;

	bool16 fFilterEnabled_ActionName;
	PMString fOrigActionName, fNewActionName; 

	bool16 fFilterEnabled_ActionArea;
	PMString fOrigActionArea, fNewActionArea;

	bool16 fFilterEnabled_ActionType;
	int16 fOrigActionType, fNewActionType;

	bool16 fFilterEnabled_EnablingType;
	uint32 fOrigEnablingType, fNewEnablingType;

	bool16 fFilterEnabled_UserEditable;
	bool16 fOrigUserEditable, fNewUserEditable;
};

// Declare ActionFilterHelper as an object type (for K2Vector)
DECLARE_OBJECT_TYPE(ActionFilterHelper);

/** Global variable that keeps a vector of ActionFilterHelpers.
 */
extern K2Vector<ActionFilterHelper> gActionFilterHelpers;

#endif // #ifndef __ActionFilterHelper_H__

// End, ActionFilterHelper.h.
