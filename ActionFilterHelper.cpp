//========================================================================================
//  
//  $File: //depot/indesign_3.x/dragonfly/source/sdksamples/customactionfilter/ActionFilterHelper.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: jzhou $
//  
//  $DateTime: 2004/03/05 12:07:45 $
//  
//  $Revision: #1 $
//  
//  $Change: 250199 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActionComponent.h"

// General includes:
#include "ActionFilterHelper.h"
#include "CreateObject.h"


/* Global variable that keeps a vector of ActionFilterHelpers.
 */
K2Vector<ActionFilterHelper> gActionFilterHelpers;

/* SetFilterComponentClass
*/
void ActionFilterHelper::SetFilterComponentClass(const bool16 enableFilter, 
												 const ClassID& origComponentClass /* = kClassIDDONTCARE */, 
												 const ClassID& newComponentClass /*= kClassIDDONTCARE */, 
												 const bool16 isKey	/* = kFalse */)
{
	this->fFilterEnabled_ComponentClass = enableFilter;
	if (this->fFilterEnabled_ComponentClass == kTrue)
	{
		// enable this filter - set values
		this->fOrigComponentClass = origComponentClass;
		this->fNewComponentClass = newComponentClass;
		if (isKey == kTrue)
		{
			this->fFilterKey = kKeyComponentClass;
		}
	}
	else
	{
		// reset values
		this->fOrigComponentClass = kClassIDDONTCARE;
		this->fNewComponentClass = kClassIDDONTCARE;
		if (this->fFilterKey == kKeyComponentClass)
		{
			this->fFilterKey = kKeyDONTCARE;
		}
	}
}

/* SetFilterActionID
 */
void ActionFilterHelper::SetFilterActionID(const bool16 enableFilter, 
										   const ActionID& origActionID	/* = kActionIDDONTCARE */, 
										   const ActionID& newActionID /* = kActionIDDONTCARE */, 
										   const bool16 isKey /* = kFalse */)
{
	this->fFilterEnabled_ActionID = enableFilter;
	if (this->fFilterEnabled_ActionID == kTrue)
	{
		// enable this filter - set values
		this->fOrigActionID = origActionID;
		this->fNewActionID = newActionID;
		if (isKey == kTrue)
		{
			this->fFilterKey = kKeyActionID;
		}
	}
	else
	{
		// reset values
		this->fOrigActionID = kActionIDDONTCARE;
		this->fNewActionID = kActionIDDONTCARE;
		if (this->fFilterKey == kKeyActionID)
		{
			this->fFilterKey = kKeyDONTCARE;
		}
	}
}

/* SetFilterActionName
*/
void ActionFilterHelper::SetFilterActionName(const bool16 enableFilter, 
											 const PMString& origActionName	/* = kActionStringDONTCARE */, 
											 const PMString& newActionName /* = kActionStringDONTCARE */, 
											 const bool16 isKey	/* = kFalse */)
{
	this->fFilterEnabled_ActionName = enableFilter;
	if (this->fFilterEnabled_ActionName == kTrue)
	{
		// enable this filter - set values
		this->fOrigActionName = origActionName;
		this->fNewActionName = newActionName;
		if (isKey == kTrue)
		{
			this->fFilterKey = kKeyActionName;
		}
	}
	else
	{
		// reset values
		this->fOrigActionName = kActionStringDONTCARE;
		this->fNewActionName = kActionStringDONTCARE;
		if (this->fFilterKey == kKeyActionName)
		{
			this->fFilterKey = kKeyDONTCARE;
		}
	}
}

/* SetFilterActionArea
*/
void ActionFilterHelper::SetFilterActionArea(const bool16 enableFilter, 
											 const PMString& origActionArea	/* = kActionStringDONTCARE */, 
											 const PMString& newActionArea /* = kActionStringDONTCARE */, 
											 const bool16 isKey	/* = kFalse */)
{
	this->fFilterEnabled_ActionArea = enableFilter;
	if (this->fFilterEnabled_ActionArea == kTrue)
	{
		// enable this filter - set values
		this->fOrigActionArea = origActionArea;
		this->fNewActionArea = newActionArea;
		if (isKey == kTrue)
		{
			this->fFilterKey = kKeyActionArea;
		}
	}
	else
	{
		// reset values
		this->fOrigActionArea = kActionStringDONTCARE;
		this->fNewActionArea = kActionStringDONTCARE;
		if (this->fFilterKey == kKeyActionArea)
		{
			this->fFilterKey = kKeyDONTCARE;
		}
	}
}

/* SetFilterActionType
*/
void ActionFilterHelper::SetFilterActionType(const bool16 enableFilter, 
											 const int16 origActionType	/* = kActionTypeDONTCARE */, 
											 const int16 newActionType /* = kActionTypeDONTCARE */, 
											 const bool16 isKey	/* = kFalse */)
{
	this->fFilterEnabled_ActionType = enableFilter;
	if (this->fFilterEnabled_ActionType == kTrue)
	{
		// enable this filter - set values
		this->fOrigActionType = origActionType;
		this->fNewActionType = newActionType;
		if (isKey == kTrue)
		{
			this->fFilterKey = kKeyActionType;
		}
	}
	else
	{
		// reset values
		this->fOrigActionType = kActionTypeDONTCARE;
		this->fNewActionType = kActionTypeDONTCARE;
		if (this->fFilterKey == kKeyActionType)
		{
			this->fFilterKey = kKeyDONTCARE;
		}
	}
}

/* SetFilterEnablingType
*/
void ActionFilterHelper::SetFilterEnablingType(const bool16 enableFilter, 
											   const uint32 origEnablingType /* = kEnablingTypeDONTCARE */, 
											   const uint32 newEnablingType	/* = kEnablingTypeDONTCARE */, 
											   const bool16 isKey /* = kFalse */)
{
	this->fFilterEnabled_EnablingType = enableFilter;
	if (this->fFilterEnabled_EnablingType == kTrue)
	{
		// enable this filter - set values
		this->fOrigEnablingType = origEnablingType;
		this->fNewEnablingType = newEnablingType;
		if (isKey == kTrue)
		{
			this->fFilterKey = kKeyEnablingType;
		}
	}
	else
	{
		// reset values
		this->fOrigEnablingType = kEnablingTypeDONTCARE;
		this->fNewEnablingType = kEnablingTypeDONTCARE;
		if (this->fFilterKey == kKeyEnablingType)
		{
			this->fFilterKey = kKeyDONTCARE;
		}
	}
}

/* SetFilterUserEditable
*/
void ActionFilterHelper::SetFilterUserEditable(const bool16 enableFilter, 
											   const bool16 origUserEditable /* = kUserEditableDONTCARE */, 
											   const bool16 newUserEditable	/* = kUserEditableDONTCARE */, 
											   const bool16 isKey /* = kFalse */)
{
	this->fFilterEnabled_UserEditable = enableFilter;
	if (this->fFilterEnabled_UserEditable == kTrue)
	{
		// enable this filter - set values
		this->fOrigUserEditable = origUserEditable;
		this->fNewUserEditable = newUserEditable;
		if (isKey == kTrue)
		{
			this->fFilterKey = kKeyUserEditable;
		}
	}
	else
	{
		// reset values
		this->fOrigUserEditable = kUserEditableDONTCARE;
		this->fNewUserEditable = kUserEditableDONTCARE;
		if (this->fFilterKey == kKeyUserEditable)
		{
			this->fFilterKey = kKeyDONTCARE;
		}
	}
}

/* DoFilter
*/
void ActionFilterHelper::DoFilter(ClassID* componentClass, 
								  ActionID* actionID, 
								  PMString* actionName, 
								  PMString* actionArea, 
								  int16* actionType, 
								  uint32* enablingType, 
								  bool16* userEditable)
{
	// filter only if there is a match
	if (this->IsMatch(*componentClass, *actionID, *actionName, 
					  *actionArea,*actionType, *enablingType, 
					  *userEditable) == kTrue)
	{
		// filter those fields that are enabled.

		if (this->IsFilterComponentClassEnabled() == kTrue)
		{
			*componentClass = fNewComponentClass;
		}

		if (this->IsFilterActionIDEnabled() == kTrue)
		{
			*actionID = fNewActionID;
		}

		if (this->IsFilterActionNameEnabled() == kTrue)
		{
			*actionName = fNewActionName;
		}

		if (this->IsFilterActionAreaEnabled() == kTrue)
		{
			*actionArea = fNewActionArea;
		}

		if (this->IsFilterActionTypeEnabled() == kTrue)
		{
			*actionType = fNewActionType;
		}

		if (this->IsFilterEnablingTypeEnabled() == kTrue)
		{
			*enablingType = fNewEnablingType;
		}

		if (this->IsFilterUserEditableEnabled() == kTrue)
		{
			*userEditable = fNewUserEditable;
		}
	}
}


/* IsMatch
*/
bool16 ActionFilterHelper::IsMatch(const ClassID& componentClass, 
								   const ActionID& actionID, 
								   const PMString& actionName, 
								   const PMString& actionArea,
								   const int16 actionType, 
								   const uint32 enablingType, 
								   const bool16 userEditable)
{
	bool16 matchFound = kFalse;
	switch (fFilterKey)
	{
	case kKeyComponentClass:
		if (componentClass == this->fOrigComponentClass)
		{
			matchFound = kTrue;
		}
		break;
	case kKeyActionID:
		if (actionID == this->fOrigActionID)
		{
			matchFound = kTrue;
		}
		break;
	case kKeyActionName: 
		if (actionName == this->fOrigActionName)
		{
			matchFound = kTrue;
		}
		break;
	case kKeyActionArea: 
		if (actionArea == this->fOrigActionArea)
		{
			matchFound = kTrue;
		}
		break;
	case kKeyActionType:
		if (actionType == this->fOrigActionType)
		{
			matchFound = kTrue;
		}
		break;
	case kKeyEnablingType: 
		if (enablingType == this->fOrigEnablingType)
		{
			matchFound = kTrue;
		}
		break;
	case kKeyUserEditable:
		if (userEditable == this->fOrigUserEditable)
		{
			matchFound = kTrue;
		}
		break;

	case kKeyDONTCARE:
	default:
		ASSERT_FAIL("[ActionFilterHelper::IsMatch()] No filter key set - no match will be found.");
		break;
	}
	return matchFound;
}

/* QueryOrigActionComponent
*/
IActionComponent* ActionFilterHelper::QueryOrigActionComponent(void)
{
	// we want the origComponentClass
	if (fOrigActionComponent == nil && 
		fOrigComponentClass != kClassIDDONTCARE)
	{
		// instantiate the action component (only once)
		fOrigActionComponent = CreateObject2<IActionComponent>(fOrigComponentClass);
		ASSERT(fOrigActionComponent);
		// increment reference count
		fOrigActionComponent->AddRef();
	}
	// this may be nil... the caller should check!
	return fOrigActionComponent;
}

// End, ActionFilterHelper.cpp.

